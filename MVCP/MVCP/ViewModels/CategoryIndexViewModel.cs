﻿using MVCP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCP.ViewModels
{
    public class CategoryIndexViewModel
    {
        public IEnumerable<Category> Category { set; get; }
        public string PageTitle { get;set; }
    }
}
