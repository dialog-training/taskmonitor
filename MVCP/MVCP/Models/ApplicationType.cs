﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVCP.Models
{
    public class ApplicationType
    {
        [Key]
        public int CateogryId { get; set; }
        public string Name { get; set; }

        public int DisplayOrder { get; set; }

    }
}
