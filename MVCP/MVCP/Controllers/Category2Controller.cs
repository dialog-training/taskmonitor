﻿using Microsoft.AspNetCore.Mvc;
using MVCP.Data;
using MVCP.Models;
using MVCP.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCP.Controllers
{
    public class Category2Controller : Controller
    {

        private readonly ApplicationDbContext _db;

        public Category2Controller(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            IEnumerable < Category > objList = _db.Category;
            
            CategoryIndexViewModel categoryIndexViewModel = new()
            {
                Category = _db.Category,
                PageTitle = "Using ViewModel<>"
            };
            ViewData["Category"] = objList;
            ViewData["PageTitle"] = "Category List";
            ViewBag.Category2 = objList;
            ViewBag.PageTitle2 = "Using ViewBag.";

            return View(categoryIndexViewModel);
        }

        //********************************************************************
        //We can specify the route for the view or controller action. For view "return View("~/Views/Category/Index.cshtml")"
        //For controller action "[Route("Category2/Index")]"
        //For controller action "[Route("Category2/[action]")]"
        //Route attribute can be applied on the controller class


        //GET for create
        public IActionResult Create()
        {
            
            return View();
        }

        //POST for create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Category obj)
        {
            if (ModelState.IsValid)
            {
                _db.Category.Add(obj);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(obj);
        }

        //GET - EDIT
        public IActionResult Edit(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var obj = _db.Category.Find(id);
            if (obj == null)
            {
                return NotFound();
            }

            return View(obj);
        }

        //POST - EDIT
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Category obj)
        {
            if (ModelState.IsValid)
            {
                _db.Category.Update(obj);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(obj);

        }

        //GET - DELETE
        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var obj = _db.Category.Find(id);
            if (obj == null)
            {
                return NotFound();
            }

            return View(obj);
        }

        //POST - DELETE
        [HttpPost]

        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(Category obj)
        {
            //var obj1 = _db.Category.Find(obj.CateogryId);
            if (obj == null)
            {
                return NotFound();
            }
            _db.Category.Remove(obj);
            _db.SaveChanges();
            return RedirectToAction("Index");


        }
    }
}
